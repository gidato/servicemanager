<?php

namespace GidatoTest\ServiceManager\TestClass;

class ClassWithBuiltinArgument
{
    public $a;
    public $b;

    public function __construct(ClassWithNoConstructor $a, array $b, ?UnknownClass $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

}
