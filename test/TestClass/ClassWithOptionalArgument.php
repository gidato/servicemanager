<?php

namespace GidatoTest\ServiceManager\TestClass;

class ClassWithOptionalArgument
{
    public $a;
    public $b;

    public function __construct(ClassWithNoConstructor $a, ?ClassWithEmptyConstructor $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

}
