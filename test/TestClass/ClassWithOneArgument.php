<?php

namespace GidatoTest\ServiceManager\TestClass;

class ClassWithOneArgument
{
    public $a;

    public function __construct(ClassWithNoConstructor $a)
    {
        $this->a = $a;
    }

}
