<?php
/**
 * @see      http://github.com/gidato/servicemanager for the canonical source repository
 * @copyright Copyright (c) 2019 Gidato Ltd. (http://www.gidato.com)
 */

namespace GidatoTest\ServiceManager;

use Gidato\ServiceManager\Factory\ReflectionFactory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Interop\Container\ContainerInterface;

class CreationTest extends TestCase
{

    /** @var ReflectionFactory */
    private $factory;
    public function setUp()
    {
        $this->factory = new ReflectionFactory();
    }

    public function testCreationWithOptions()
    {
        $this->expectException(ServiceNotFoundException::class);
        $this->expectExceptionMessage(sprintf(
            'Reflection Factory cannot be used with options for requested service "%s"',
            TestClass\ClassWithNoConstructor::class
        ));
        $container = $this->createMock(ContainerInterface::class);
        $object = ($this->factory)($container, TestClass\ClassWithNoConstructor::class, ['hello']);
    }

    public function testClassNotFound()
    {
        $this->expectException(ServiceNotFoundException::class);
        $this->expectExceptionMessage(sprintf(
            'A class for the requested service "%s" could not be found',
            TestClass\Unknown::class
        ));
        $container = $this->createMock(ContainerInterface::class);
        $object = ($this->factory)($container, TestClass\Unknown::class);
    }

    public function testNoConstructor()
    {
        $container = $this->createMock(ContainerInterface::class);
        $object = ($this->factory)($container, TestClass\ClassWithNoConstructor::class);
        $this->assertInstanceof(TestClass\ClassWithNoConstructor::class, $object);
    }

    public function testEmptyConstructor()
    {
        $container = $this->createMock(ContainerInterface::class);
        $object = ($this->factory)($container, TestClass\ClassWithEmptyConstructor::class);
        $this->assertInstanceof(TestClass\ClassWithEmptyConstructor::class, $object);
    }

    public function testConstructorWithOneClassArgument()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)
                          ->setMethods(['has','get'])
                          ->getMock();

        $a = $this->createMock(TestClass\ClassWithNoConstructor::class);

        $container->expects($this->once())
                  ->method('has')
                  ->with($this->equalTo(TestClass\ClassWithNoConstructor::class))
                  ->will($this->returnValue(true));

        $container->expects($this->once())
                  ->method('get')
                  ->with($this->equalTo(TestClass\ClassWithNoConstructor::class))
                  ->will($this->returnValue($a));

        $object = ($this->factory)($container, TestClass\ClassWithOneArgument::class);
        $this->assertInstanceof(TestClass\ClassWithOneArgument::class, $object);
        $this->assertSame($a, $object->a);
    }


    public function testConstructorWithMoreThanOneClassArgument()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)
                          ->setMethods(['has','get'])
                          ->getMock();

        $a = $this->createMock(TestClass\ClassWithNoConstructor::class);
        $b = $this->createMock(TestClass\ClassWithEmptyConstructor::class);
        $has = [
            [TestClass\ClassWithNoConstructor::class, true],
            [TestClass\ClassWithEmptyConstructor::class, true],
        ];
        $get = [
            [TestClass\ClassWithNoConstructor::class, $a],
            [TestClass\ClassWithEmptyConstructor::class, $b],
        ];

        $container->expects($this->exactly(2))
                  ->method('has')
                  ->will($this->returnValueMap($has));

        $container->expects($this->exactly(2))
                  ->method('get')
                  ->will($this->returnValueMap($get));

        $object = ($this->factory)($container, TestClass\ClassWithMoreThanOneArgument::class);
        $this->assertInstanceof(TestClass\ClassWithMoreThanOneArgument::class, $object);
        $this->assertSame($a, $object->a);
        $this->assertSame($b, $object->b);
    }

    public function testConstructorWithMoreThanOneClassArgumentAndOneNotFound()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)
                          ->setMethods(['has','get'])
                          ->getMock();

        $a = $this->createMock(TestClass\ClassWithNoConstructor::class);

        $has = [
            [TestClass\ClassWithNoConstructor::class, true],
            [TestClass\ClassWithEmptyConstructor::class, false],
        ];
        $get = [
            [TestClass\ClassWithNoConstructor::class, $a],
        ];

        $container->expects($this->exactly(2))
                  ->method('has')
                  ->will($this->returnValueMap($has));

        $container->expects($this->exactly(1))
                  ->method('get')
                  ->will($this->returnValueMap($get));

        $this->expectException(ServiceNotFoundException::class);
        $this->expectExceptionMessage(sprintf(
            'Cannot find dependency for "%s" when creating "%s"',
            TestClass\ClassWithEmptyConstructor::class,
            TestClass\ClassWithMoreThanOneArgument::class
        ));
        $object = ($this->factory)($container, TestClass\ClassWithMoreThanOneArgument::class);
    }

    public function testConstructorWithMoreThanOneClassArgumentAndOneNotFoundButOptional()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)
                          ->setMethods(['has','get'])
                          ->getMock();

        $a = $this->createMock(TestClass\ClassWithNoConstructor::class);

        $has = [
            [TestClass\ClassWithNoConstructor::class, true],
            [TestClass\ClassWithEmptyConstructor::class, false],
        ];
        $get = [
            [TestClass\ClassWithNoConstructor::class, $a],
        ];

        $container->expects($this->exactly(2))
                  ->method('has')
                  ->will($this->returnValueMap($has));

        $container->expects($this->exactly(1))
                  ->method('get')
                  ->will($this->returnValueMap($get));

        $object = ($this->factory)($container, TestClass\ClassWithOptionalArgument::class);
        $this->assertInstanceof(TestClass\ClassWithOptionalArgument::class, $object);
        $this->assertSame($a, $object->a);
        $this->assertNull($object->b);
    }

    public function testConstructorWithParameterNotTypeHinted()
    {
        $this->expectException(ServiceNotFoundException::class);
        $this->expectExceptionMessage(sprintf(
            'Reflection Factory can only be used when each parameter is type hinted. (%s)',
            TestClass\ClassWithParameterNotTypeHinted::class
        ));
        $container = $this->createMock(ContainerInterface::class);
        $object = ($this->factory)($container, TestClass\ClassWithParameterNotTypeHinted::class);

    }

    public function testConstructorWithBuiltinType()
    {
        $this->expectException(ServiceNotFoundException::class);
        $this->expectExceptionMessage(sprintf(
            'Reflection Factory can only be used when each parameter is an object and not built-in. (%s)',
            TestClass\ClassWithBuiltinArgument::class
        ));
        $container = $this->getMockBuilder(ContainerInterface::class)
                          ->setMethods(['has','get'])
                          ->getMock();

        $a = $this->createMock(TestClass\ClassWithNoConstructor::class);

        $has = [
            [TestClass\ClassWithNoConstructor::class, true],
        ];
        $get = [
            [TestClass\ClassWithNoConstructor::class, $a],
        ];

        $container->expects($this->exactly(1))
                  ->method('has')
                  ->will($this->returnValueMap($has));

        $container->expects($this->exactly(1))
                  ->method('get')
                  ->will($this->returnValueMap($get));

        $object = ($this->factory)($container, TestClass\ClassWithBuiltinArgument::class);

    }

}
