<?php
/**
 * @link      http://github.com/gidato/servicemanager for the canonical source repository
 * @copyright Copyright (c) 2019 Gidato Ltd. (http://www.gidato.com)
 */

namespace Gidato\ServiceManager\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\Exception;
use Interop\Container\ContainerInterface;
use ReflectionMethod;
use ReflectionParameter;

/**
 * Factory for instantiating classes where the required dependencies can be inferred from the classes in the constuctor.
 *
 * The ReflectionFactory can be used for any class that:
 *
 * - has no constructor arguments;
 * - has arguments that are all classes or interfaces, and each class/interface has an entry in the container.
 */
final class ReflectionFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (!empty($options)) {
            throw new Exception\ServiceNotFoundException(
                sprintf('Reflection Factory cannot be used with options for requested service "%s"', $requestedName)
            );
        }

        if (!class_exists($requestedName)) {
            throw new Exception\ServiceNotFoundException(
                sprintf('A class for the requested service "%s" could not be found', $requestedName)
            );
        }

        $dependencies = $this->generateDependencies($container, $requestedName);
        return new $requestedName(...$dependencies);
    }

    private function generateDependencies(ContainerInterface $container, $requestedName) : array
    {

        if (!method_exists($requestedName, '__construct')) {
            return [];
        }

        $constructor = new ReflectionMethod($requestedName, '__construct');
        $parameters = $constructor->getParameters();
        $dependencies = [];
        foreach ($parameters as $parameter) {
            $dependencies[$parameter->getPosition()] = $this->generateDependentClassForParameter($container, $parameter, $requestedName);
        }
        ksort($dependencies);
        return $dependencies;
    }

    private function generateDependentClassForParameter(ContainerInterface $container, ReflectionParameter $parameter, $requestedName)
    {
        if (!$parameter->hasType()) {
            throw new Exception\ServiceNotFoundException(
                sprintf('Reflection Factory can only be used when each parameter is type hinted. (%s)', $requestedName)
            );
        }

        $parameterType = $parameter->getType();
        if ($parameterType->isBuiltin()) {
            throw new Exception\ServiceNotFoundException(
                sprintf('Reflection Factory can only be used when each parameter is an object and not built-in. (%s)', $requestedName)
            );
        }

        $typeName = (string) $parameterType;

        if ($container->has($typeName)) {
            return $container->get($typeName);
        }

        if ($parameterType->allowsNull()) {
            return null;
        }

        throw new Exception\ServiceNotFoundException(
            sprintf('Cannot find dependency for "%s" when creating "%s"', $typeName, $requestedName)
        );

    }
}
